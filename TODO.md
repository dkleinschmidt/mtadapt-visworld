# Required fixes

1. Make stimuli fit on small screen (reduce image size? reduce container size?)
2. Add instructions
   * General boilerplate (consent, technical difficulty, etc.)
   * Task-specific instructions (use headphones, use a mouse please, how long it will be)
3. Add final surveys         
4. Add instructions during familiarization ('click image to advance')
